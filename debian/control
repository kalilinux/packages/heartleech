Source: heartleech
Section: utils
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>,
           Ben Wilson <g0tmi1k@kali.org>,
Build-Depends: debhelper-compat (= 13),
               ca-certificates,
               git,
Standards-Version: 4.6.2
Homepage: https://github.com/robertdavidgraham/heartleech
Vcs-Git: https://gitlab.com/kalilinux/packages/heartleech.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/heartleech

Package: heartleech
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         kali-defaults,
Description: Scanner detecting systems vulnerable to the heartbleed OpenSSL bug
 This is a typical "heartbleed" tool. It can scan for systems vulnerable
 to the bug, and then be used to download them. Some important features:
 .
  * conclusive/inconclusive verdicts as to whether the target is vulnerable
  * bulk/fast download of heartbleed data into a large files for offline
    processing using many threads
  * automatic retrieval of private keys with no additional steps
  * some limited IDS evasion
  * STARTTLS support
  * IPv6 support
  * Tor/Socks5n proxy support
  * extensive connection diagnostic information
